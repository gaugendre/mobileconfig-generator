# Mobileconfig generator

Generate a valid mobileconfig profile to setup your email account on all your Apple devices.

Everything happens in your browser.

An example with Python installed on your machine:

* clone the repository
* cd to the directory
* start a webserver pointing to your current directory: `python -m http.server`
* open http://localhost:8000 in your browser


# Reuse
If you do reuse my work, please consider linking back to this repository 🙂